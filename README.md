# Code Quality

Use Code Quality to analyze your source code's quality and complexity. This helps keep your project's code simple, readable, and easier to maintain. Code Quality should supplement your other review processes, not replace them.

Code Quality uses the open source Code Climate tool, and selected [plugins](https://docs.codeclimate.com/docs/list-of-engines)
to analyze your source code.
To confirm if your code's languages are covered, see the Code Climate list of [Supported Languages for Maintainability](https://docs.codeclimate.com/docs/supported-languages-for-maintainability).
You can extend the code coverage either by using [Code Climate Analysis Plugins](https://docs.codeclimate.com/docs/list-of-engines) or a [custom tool](https://docs.gitlab.com/ee/ci/testing/code_quality.html#implement-a-custom-tool).

Run Code Quality reports in your CI/CD pipeline to verify changes don't degrade your code's quality, before committing them to the default branch.

Learn more at https://docs.gitlab.com/ee/ci/testing/code_quality.html

## Usage

Use this component to scan your source code and get a report about code quality and complexity.

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/components/code-quality/code-quality@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

This adds a job called `code_quality` to the pipeline.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test`      | The stage where you want the job to be added |
| `image` | `$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/ci-cd/codequality:0.96.0` | The Docker image of the analyzer |
| `debug` | `""`        | Set to `1` to enable [CodeClimate debug mode](https://github.com/codeclimate/codeclimate#environment-variables) |

### Variables

You can customize the code quality analyzer by defining the following CI/CD variables:

| CI/CD variable | Description |
| -------------- | ----------- |
| `SOURCE_CODE` | Path to the source code to scan |
| `TIMEOUT_SECONDS` | Custom timeout per engine container for the codeclimate analyze command, default is 900 seconds (15 minutes) |
| `CODECLIMATE_DEV` |	Set to enable `--dev` mode which lets you run engines not known to CodeClimate CLI |
| `REPORT_STDOUT` | Set to print the report to `STDOUT` instead of generating the usual report file |
| `REPORT_FORMAT`	| Set to control the format of the generated report file. One of: `json`, `html` |
| `ENGINE_MEMORY_LIMIT_BYTES` |	Set the memory limit for per engine per bytes, default is 1,024,000,000 bytes |
| `CODE_QUALITY_DISABLED`	| Prevents the Code Quality job from running. |
| `CODECLIMATE_PREFIX` | Set a prefix to use with all docker pull commands in CodeClimate engines. Useful for [offline scanning](https://github.com/codeclimate/codeclimate/pull/948) |
| `CODECLIMATE_REGISTRY_USERNAME` | An optional variable to specify the username for the registry domain parsed from `CODECLIMATE_PREFIX` |

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components 
