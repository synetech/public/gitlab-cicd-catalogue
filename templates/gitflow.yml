spec:
  inputs:
    stage:
      default: deploy
    version:
      description: "Script that provides the version that will be used for actions (e.g. node -p \"require('./package.json').version\")"
    should_bump_version:
      default: false
      type: boolean
    version_bump_script:
      description: "Script that bumps project version after release branch is created"
      default: undefined
    dev_branch:
      description: "The name of the development branch (e.g. dev)"
      default: dev
    main_branch:
      description: "The name of the main branch (e.g. main)"
      default: main
    additional_tools:
      description: "Additional tools to install"
      default: ""

---

createReleaseBranch:
  stage: $[[ inputs.stage ]]
  rules:
    - if: $CI_COMMIT_REF_NAME == "$[[ inputs.dev_branch ]]"
      when: manual
      allow_failure: true
  image: ubuntu:latest
  before_script:
    - apt-get update
    - apt-get install git $[[ inputs.additional_tools ]] -y
  script:
    # Check if release branch already exists
    - if ((git fetch origin --unshallow || git fetch origin) && git --no-pager branch --list --remotes | grep -q "origin/release"); then echo "Release branch already exists"; exit 1; fi

    # Get the version
    - version=$($[[ inputs.version ]])
    
    # Create a release branch
    - git checkout -b "release/$version"

    # Get the URL of the origin remote of the git repository
    - originURL=$(git remote get-url origin | tr -d '\n')

    # Extract the part of the URL after the '@' character
    - originURLWithToken=${originURL#*@}

    # Push to the repository using the bot's credentials
    - git push "https://synetech_bot:${GITLAB_BOT_PUSH_TOKEN}@${originURLWithToken}" "release/$version" > /dev/null 2>&1

    # Bump version
    - | 
      if [ "$[[ inputs.should_bump_version ]]" != "false" ]; then 
        git checkout $CI_COMMIT_REF_NAME
        $($[[ inputs.version_bump_script ]])
        git config user.email "bot@megumethod.com"
        git config user.name "Bot @ MeguMethod"
        version=$($[[ inputs.version ]])
        git add .
        git commit -m "BUMP $version"
        git push "https://synetech_bot:${GITLAB_BOT_PUSH_TOKEN}@${originURLWithToken}" "$CI_COMMIT_REF_NAME" -o ci.skip > /dev/null 2>&1
      fi

releaseVersion:
  stage: $[[ inputs.stage ]]
  rules:
    - if: '$CI_COMMIT_REF_NAME == "release" || $CI_COMMIT_REF_NAME =~ /^release/'
      when: manual
  image: ubuntu:latest
  before_script:
    - apt-get update
    - apt-get install git curl $[[ inputs.additional_tools ]] -y
  script:
    # Get the URL of the origin remote of the git repository
    - originURL=$(git remote get-url origin | tr -d '\n')

    # Extract the part of the URL after the '@' character
    - originURLWithToken=${originURL#*@}

    # Fastforward the main branch to the release branch
    - git fetch origin --unshallow || git fetch origin
    - git checkout "$[[ inputs.main_branch ]]" -f
    - git reset HEAD --hard 
    - git merge "origin/$CI_COMMIT_REF_NAME" --ff-only
    - git push "https://synetech_bot:${GITLAB_BOT_PUSH_TOKEN}@${originURLWithToken}" "$[[ inputs.main_branch ]]" > /dev/null 2>&1

    # Get the version 
    - version=$($[[ inputs.version ]])

    # Create a tag for the version
    - git tag "v$version" -f

    # Push to the repository using the bot's credentials
    - git push "https://synetech_bot:${GITLAB_BOT_PUSH_TOKEN}@${originURLWithToken}" "v$version" > /dev/null 2>&1

    # Create a merge request to dev branch
    - |
      curl $CI_API_V4_URL/projects/$CI_PROJECT_ID/merge_requests \
        -X POST \
        -H "Content-Type: application/json" \
        -H "PRIVATE-TOKEN: $GITLAB_BUILD_API_TOKEN" \
        -d "{\"id\": $CI_PROJECT_ID, \"source_branch\": \"$CI_COMMIT_REF_NAME\", \"target_branch\": \"$[[ inputs.dev_branch ]]\", \"title\": \"Release $version\", \"remove_source_branch\": true}"

create-release:
  stage: $[[ inputs.stage ]]
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v?(\d+\.)?(\d+\.)?(\d+)$/'
  before_script:
    - ""
  script: echo "Creating release $CI_COMMIT_TAG"
  release:
    tag_name: $CI_COMMIT_TAG
    description: "Release $CI_COMMIT_TAG of components repository $CI_PROJECT_PATH"
